#include <iostream>
using namespace std;

int main() {
    double nilai, quiz, absen, uts, uas, tugas;
    char Huruf_Mutu;

    // Meminta input dari pengguna untuk nilai-nilai
    cout << "Masukkan nilai Absen: ";
    cin >> absen;
    cout << "Masukkan nilai Tugas: ";
    cin >> tugas;
    cout << "Masukkan nilai Quiz: ";
    cin >> quiz;
    cout << "Masukkan nilai UTS: ";
    cin >> uts;
    cout << "Masukkan nilai UAS: ";
    cin >> uas;

    // Meminta input dari pengguna untuk persentase bobot
    double persentase_absen, persentase_tugas, persentase_quiz, persentase_uts, persentase_uas;
    cout << "Masukkan persentase bobot Absen (dalam desimal): ";
    cin >> persentase_absen;
    cout << "Masukkan persentase bobot Tugas (dalam desimal): ";
    cin >> persentase_tugas;
    cout << "Masukkan persentase bobot Quiz (dalam desimal): ";
    cin >> persentase_quiz;
    cout << "Masukkan persentase bobot UTS (dalam desimal): ";
    cin >> persentase_uts;
    cout << "Masukkan persentase bobot UAS (dalam desimal): ";
    cin >> persentase_uas;

    // Menghitung nilai berdasarkan bobot yang diinputkan oleh pengguna
    nilai = ((persentase_absen * absen) + (persentase_tugas * tugas) + (persentase_quiz * quiz) + (persentase_uts * uts) + (persentase_uas * uas)) / 2;

    // Menentukan Huruf Mutu
    if (nilai > 85 && nilai <= 100)
        Huruf_Mutu = 'A';
    else if (nilai > 70 && nilai <= 85)
        Huruf_Mutu = 'B';
    else if (nilai > 55 && nilai <= 70)
        Huruf_Mutu = 'C';
    else if (nilai > 40 && nilai <= 55)
        Huruf_Mutu = 'D';
    else if (nilai >= 0 && nilai <= 40)
        Huruf_Mutu = 'E';

    // Menampilkan hasil
    cout << "Huruf Mutu : " << Huruf_Mutu << endl;

    return 0;
}

